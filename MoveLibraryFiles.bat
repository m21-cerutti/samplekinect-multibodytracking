if not exist Assets\Plugins\Kinect mkdir Assets\Plugins\Kinect
if not exist Build\ mkdir Build\

set BODY_TRACKING_SDK_PATH=C:\Program Files\Azure Kinect Body Tracking SDK\
set BODY_TRACKING_TOOLS_PATH="%BODY_TRACKING_SDK_PATH%tools\"
set BODY_TRACKING_LIB_PATH="%BODY_TRACKING_SDK_PATH%sdk\netstandard2.0\release\"
set UNITY_PLUGINS_PATH="Assets\Plugins\Kinect"

copy packages\System.Buffers.4.5.1\lib\netstandard2.0\System.Buffers.dll %UNITY_PLUGINS_PATH%
copy packages\System.Memory.4.5.4\lib\netstandard2.0\System.Memory.dll %UNITY_PLUGINS_PATH%
copy packages\System.Runtime.CompilerServices.Unsafe.6.0.0\lib\netstandard2.0\System.Runtime.CompilerServices.Unsafe.dll %UNITY_PLUGINS_PATH%
copy packages\System.Reflection.Emit.Lightweight.4.7.0\lib\netstandard2.0\System.Reflection.Emit.Lightweight.dll %UNITY_PLUGINS_PATH%

copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\netstandard2.0\Microsoft.Azure.Kinect.Sensor.dll %UNITY_PLUGINS_PATH%
copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\netstandard2.0\Microsoft.Azure.Kinect.Sensor.pdb %UNITY_PLUGINS_PATH%
copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\netstandard2.0\Microsoft.Azure.Kinect.Sensor.deps.json %UNITY_PLUGINS_PATH%
copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\netstandard2.0\Microsoft.Azure.Kinect.Sensor.xml %UNITY_PLUGINS_PATH%

copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\native\amd64\release\depthengine_2_0.dll %UNITY_PLUGINS_PATH%
copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\native\amd64\release\k4a.dll %UNITY_PLUGINS_PATH%
copy packages\Microsoft.Azure.Kinect.Sensor.1.4.1\lib\native\amd64\release\k4arecord.dll %UNITY_PLUGINS_PATH%

copy %BODY_TRACKING_LIB_PATH%Microsoft.Azure.Kinect.BodyTracking.dll %UNITY_PLUGINS_PATH%
copy %BODY_TRACKING_LIB_PATH%Microsoft.Azure.Kinect.BodyTracking.pdb %UNITY_PLUGINS_PATH%
copy %BODY_TRACKING_LIB_PATH%Microsoft.Azure.Kinect.BodyTracking.deps.json %UNITY_PLUGINS_PATH%
copy %BODY_TRACKING_LIB_PATH%Microsoft.Azure.Kinect.BodyTracking.xml %UNITY_PLUGINS_PATH%

copy %BODY_TRACKING_TOOLS_PATH%*.dll %UNITY_PLUGINS_PATH%
copy %BODY_TRACKING_TOOLS_PATH%*.dll ./
copy %BODY_TRACKING_TOOLS_PATH%*.onnx %UNITY_PLUGINS_PATH%
copy %BODY_TRACKING_TOOLS_PATH%*.onnx ./

copy %BODY_TRACKING_TOOLS_PATH%cudnn64_8.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cudnn_cnn_infer64_8.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cudnn_ops_infer64_8.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%onnxruntime.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%dnn_model_2_0_op11.onnx .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cublas64_11.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cublasLt64_11.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cudart64_110.dll .\Build\
copy %BODY_TRACKING_TOOLS_PATH%cufft64_10.dll .\Build\